package com.availity.spark.provider

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructType, StructField,DateType, StringType}
import org.apache.spark.sql.functions.{count, lit, array, collect_list, col, month, avg, date_format,to_date}

object ProviderRoster  {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().master("local[2]").getOrCreate();

    val prov = spark.read.format("csv").option("header", "true").option("delimiter","|").load("/Users/gkatta/Downloads/providers.csv")
    val customSchema = StructType(Array(
         StructField("visit_id", StringType, true),
         StructField("provider_id", StringType, true),
         StructField("visit_date", DateType, true)
         ))
    val visits = spark.read.format("csv").schema(customSchema).load("/Users/gkatta/Downloads/visits.csv")
    val num_visits = prov.join(visits,"provider_id")
    val num_visits_per_prov = num_visits.groupBy("provider_id").agg(count("visit_date").alias("num_visits_val"))
    val result_1 = num_visits_per_prov.join(prov,"provider_id").select("provider_id","first_name","middle_name","last_name","provider_specialty","num_visits_val")
    result_1.coalesce(1).write.partitionBy("provider_specialty").mode("overwrite").json("/Users/gkatta/Downloads/output/result_1_scala")
    val monthly_visits = visits.withColumn("month_name", date_format(to_date(col("visit_date"), "yyyy-MM-dd"), "MMMM"))
    val num_visits_per_prov_mon = monthly_visits.groupBy("provider_id","month_name").agg(count("visit_date").alias("num_visits_val"))
    num_visits_per_prov_mon.coalesce(1).write.mode("overwrite").json("/Users/gkatta/Downloads/output/result_2_scala")


  }

}
